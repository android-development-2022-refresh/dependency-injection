---
title: Wrapup
template: main-full.html
---

We only used constructor injection in this example, but you can also
use property injection:

```kotlin
class Person {
    @Inject lateinit var phone: Phone 
}
```

When Hilt is asked to create a `Person`, it will create a `Phone` to
inject. (Note that you'll need to tell Hilt which concrete phone type
to create when asked for a `Phone` - this is similar to how we 
specified which `MovieRepository` to create.)

## Testing Notes

When unit testing, you'll use _manual_ dependency injection; you won't
need to set up Hilt modules for it. (More complex integration or UI 
automation testing may need Hilt modules). 

For example, if you wanted to test the above `Person` with an test phone,
you could write:

```kotlin
@Test
fun personTest() {
    val person = Person().apply {
        phone = TestPhone()
    }
    // ... run some tests...
}
```

or if you had defined `Person` with constructor injection:

```kotlin
@Test
fun personTest() {
    val person = Person(phone = TestPhone())
    // ... run some tests...
}
```
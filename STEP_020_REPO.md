---
title: Manual Injection
template: main-repo.html
---

Starting with our last Movie example (the one that used
LazyColumn for displaying lists of Movies, Actors and Ratings), let's
look at how the pieces depend on one another:

```mermaid
graph TD
    Database
    Repository -->|depends on| Database
    VM[View Model] -->|depends on| Repository
```

Right now, the Repository and View Model explicitly create the
instances of the Database and Repository, respectively:

```kotlin
class MovieDatabaseRepository(context: Context): MovieRepository {
    private val dao = createDao(context)
    ...
}

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: MovieRepository = MovieDatabaseRepository(application)
    ...
}
```

This setup locks in which explicit dependencies we're using. When we
wanted to use an alternative repository to access REST services,
we had to change the code in the view model:

```kotlin
class MovieViewModel(application: Application) : ScreenViewModel(application) {
    private val repository = MovieRestRepository(viewModelScope)
    ...
}
```

We can make this more flexible by passing dependencies into the
constructor or a property. 

We start by {{ find("020-inject-dao", "injecting the DAO") }} into
the repository and 
{{ find("020-inject-repo", "injecting the repository") }} into the
view model. It's a subtle change, but notice how the 
`MovieDatabaseRepository` and `MovieViewModel` no longer know what 
they're getting; they could be passed different implementations.

!!! note

    The repository still knows the _type_ of the DAO being passed to
    it, but when we create it we could pass in a different database
    file, for example, or use an in-memory database.

This makes our view model and repository more-easily testable,
as we can pass in different dependencies, such as test doubles
to isolate exactly what we're testing.

But we need to wire things together somewhere. Let's start at the
top, where we create our view model.  

Normally, the view model either has no arguments to its constructor,
or an `Application` context passed in. Because we want to pass
custom arguments, we need to 
{{ find("020-build-vm", "define a `ViewModelFactory`") }} to build our
view model.

We can then {{ find("020-use-factory-to-create-vm", "use the factory") }}
to create the view model instance.

This requires extracting how we {{ find("020-extract-repo-creation", "create the repository") }},
as we don't have access to the DAO in the UI layer (nor should we).

If we run the application, it works the same way it used to.
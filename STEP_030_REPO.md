---
title: Hilt Injection
template: main-repo.html
---

Hilt is a simplification API built on top of Dagger, a 
dependency-injection container. We'll use it to wire our pieces
together.

There are more details to Hilt for more complex use cases than we'll
use here; please see https://developer.android.com/training/dependency-injection/hilt-android for details.

## Hilt Gradle Setup
First, we need to set up Hilt in our gradle build. We start in the
root build.gradle and {{ find("030-declare-hilt", "declare which version") }} of Hilt to use.

Then, in each module that provides dependencies, we
{{ find("030-use-hilt", "apply the plugin") }},
{{ find("030-allow-refs", "allow generated code") }} to be referenced in user-written code,
and {{ find("030-hilt-deps", "add hilt library dependencies") }}.

The `kapt` dependency specifies the Hilt annotation processor, which
will look for annotations in our source and generate any needed code.

!!! note

    The same setup is done in the `repository` and `app` modules.

## The Application Class

Android provides an **Application Context** that gives us a common
context to access application details and access to the application-level
lifecycle. Hilt wants to tie into the application lifecycle, so we
need to {{ find("030-define-application", "define an Application") }} and 
{{ find("030-add-application", "add it to our manifest") }} using the `name`
attribute of the application tag.

We don't need to do anything special with the application class other
that annotate it with `@HiltAndroidApp`.

## Data Layer

In the data layer, we need to tell Hilt how to provide a DAO.

The DAO can only be accessed from a Database, so we need to tell
Hilt how to create that as well.

We'll delete the old factory function and add a 
{{ find("030-data-module", "**Hilt Module**") }} that
describes how we want Hilt to provide our dependencies.

If we need to access properties of another object or create
instances passing parameters or using a builder, we need to tell
hilt how to "provide" the object via the `@Provides` annotation.

Our {{ find("030-dao", "DAO is provided") }} from a room database.
We only ever need Hilt to grab it once, so we ask it to store it as
a singleton instance using the `@Singleton` annotation.

Providing the DAO requires a `MovieDatabase` instance, so we pass it
to the `provideMovieDao` function. Because of this, we need to tell
Hilt how to {{ find("030-database", "provide the movie database") }},
which uses the same database builder technique we've already been using.

Providing the database requires a context, so we use Hilt's
`@ApplicationContext` annotation on the context parameter to the 
`provideAppDatabase` function.

The `MovieDatabase` is also a `@Singleton` - this is critical as
Room Database objects perform caching and updating `Flow`s.

## Repository Layer

The repository layer is a little simpler. First, we tell Hilt that it
needs to use {{ find("030-inject-dao", "constructor injection") }}.
We do this by annotating the `MovieDatabaseRepository` constructor
with `@Inject`. Normally, the Kotlin primary-constructor has a
very terse syntax. To annotate, we must be more verbose, adding the 
`constructor` keyword.

Hilt will look at the types of the annotated constructor's parameters 
and create the instances, passing them to the constructor. In this case,
it sees we need a `MovieDao`, which it knows how to create because 
we set it up in the data layer.

Now we need to tell Hilt how to provide the repository when asked.

This is a little different than before. All we need to do is tell
Hilt which _concrete class_ to use when someone requests a
`MovieRepository`. To do this, we {{ find("030-repo", "create a binding") }}
rather than set up a provider.

The binding function (annotated with `@Binds`) indicates which type
it's binding as its return type (in this case, `MovieRepository`). The
concrete type is specified as the parameter (`MovieDatabaseRepository`). 
Because we added `@Inject` to the constructor of `MovieDatabaseRepository`,
Hilt knows how to create an instance of it to return when someone
asks for a `MovieRepository`.

!!! note

    We've deleted `RepositoryBuilder.kt` as we no longer need it.

## UI Layer

Now we're at the top layer, and need to set up the view model.

We {{ find("030-vm", "inject the view model constructor") }} in the 
same way we did for the `MovieDatabaseRepository`. This tells Hilt
to inject a `MovieRepository` when creating the view model (which will
result in creating a `MovieDatabaseRepository` to pass in).

We've also added a `@HiltViewModel` annotation - this allows us to 
create an instance of the view model using the 
{{ find("030-create-vm", "same technique as before") }},
but Hilt will step in and do the actual object creation for us.

Finally, we {{ find("030-entry", "mark the Activity") }} as an
entry point for Hilt to provide injection. _This is critical_. This tells
Hilt that when the Activity is created, it needs to perform injection
for its constructor, properties or in function arguments (like our `viewModel()` call).

And we're done - we've now set up dependencies in a more isolated
and automated form across our modules, and the application works the
same way it previously did.
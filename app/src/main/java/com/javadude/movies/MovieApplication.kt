package com.javadude.movies

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

// ##START 030-define-application
@HiltAndroidApp
class MovieApplication : Application()
// ##END
